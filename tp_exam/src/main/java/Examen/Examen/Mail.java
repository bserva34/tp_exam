package Examen.Examen;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Mail {
	private String adresse;
	private String titre;
	private String texte;
	private String [] piece_jointe;
	
	//Constructeur 
	public Mail(String adresse, String titre, String texte, String [] piece) {
		this.adresse = adresse;
		this.titre = titre;
		this.texte = texte;
		this.piece_jointe = piece;
		
	}
	
	//Getter and Setters
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getTexte() {
		return texte;
	}
	public void setTexte(String texte) {
		this.texte = texte;
	}

	//affichage
	@Override
	public String toString() {
		return "Mail [adresse=" + adresse + ", titre=" + titre + ", texte=" + texte + "]";
	}
	
	//Check bonne forme adresse mail
	public boolean check() {
		return this.adresse.matches("[a-z0-9.]+@[a-z]+[.][a-z]+");
	}
	
	//check mot présent dans le texte
	public boolean check_texte(String element) {
		String regex = element;
        Pattern pattern = Pattern.compile(regex);
		Matcher m = pattern.matcher(this.texte);
		if (m.find()) {
			return true;
		}
		return false;
		// deuxième option
		// this.texte.contains("le mot qu'on veut") renvoie bool
	}
	
	//présence du titre
	public boolean check_titre() {
		return this.titre.isEmpty();
	}
	
	//présence d'une pièce jointe
	public boolean check_pj() {
		return this.piece_jointe == null;
	}
	
	
	public String envoyer(String eletest) {
		if (!this.check()) {
			return "erreur de mail";
		}
		if (this.check_titre()) {
			return "pas de titre";
		}
		if (!(this.check_texte(eletest))) {
			return "pas de mot correspondant";
		}
		if (this.check_pj()) {
			return "pas de pj";
		}
		
		return "envoie effectuer";
		
	}
	
	
	
	
	
	
}
