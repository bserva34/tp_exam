package Examen.Examen;
import static org.junit.jupiter.api.Assertions.assertEquals;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;



import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


import Examen.Examen.*;


class AppTestMail{
	String[] t1 = {};
	Mail mail1 = new Mail("b@g.com","test","ben est en galère sur le tp3",t1);
	Mail mail2 = new Mail("@g.com","test","texte",t1);
	
	
	@Test 
	public void affichage_mail1() {
		assertEquals("Mail [adresse=b@g.com, titre=test, texte=ben est en galère sur le tp3]",mail1.toString());
	}
	
	@Test 
	public void affichage_mail2() {
		assertEquals("Mail [adresse=@g.com, titre=test, texte=texte]",mail2.toString());
	}
	
	@Test
	public void affichage_adresse1() {
		assertEquals(mail1.getAdresse(),"b@g.com");
	}
	
	@Test
	public void affichage_adresse2() {
		assertEquals(mail2.getAdresse(),"@g.com");
	}
	
	@Test
	public void mail1bon() {
		assertEquals(mail1.check(),true);
	}
	
	@Test
	public void mail2bon() {
		assertEquals(mail2.check(),false);
	}
	
	/*
	@ParameterizedTest
	@ValueSource(strings = {"adresses","jspme.fr","@dddf.fr"})
	void valide_mail1(String adresse) {
		mail1.setAdresse(adresse);
		assertEquals(false,mail1.check());
	}
	
	@ParameterizedTest(name="{index} - {0} should return {1}")
	@MethodSource("destinationTest")
	void destinTest(String adresse, boolean expected, Mail mail1) {
		mail1.setAdresse(adresse);
		assertEquals(expected, mail1.check());
	}
	
	private static Stream<Arguments> destinationTest(){
		String[] t1 = {};
		Mail mail1 = new Mail("b@g.com","test","ben est en galère sur le tp3",t1);
		return Stream.of(
				Arguments.of("adres@ses.iffdsdd",true,mail1)
				);
	}*/
} 