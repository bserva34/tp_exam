package Examen.Examen;


import static org.junit.jupiter.api.Assertions.assertEquals;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import Examen.Examen.*;

class AppTestLettre{
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	
	Lettre lettre1 = new Lettre("Le pere Noel",
			"famille Kirik, igloo 5, banquise nord",
			"7877", 25, 0.00018f, Recommandation.un, false);
	Lettre lettre2 = new Lettre("Le pere Noel",
			"famille Kouk, igloo 2, banquise nord", 
			"5854", 18, 0.00018f, Recommandation.deux, true);
	
	@Test 
    public void testLettre1() {
    	assertEquals("Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire",lettre1.toString());
    }
    
    @Test 
    public void testLettre2() 
    {
    	assertEquals("Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence",lettre2.toString());
    }
    
    @Test
    public void affranlettre1() {
    	
    	assertEquals(lettre1.tarifAffranchissement(),1.0f,tolerancePrix);
    }
    
    @Test
    public void affranlettre2() {
    	assertEquals(lettre2.tarifAffranchissement(),2.3f,tolerancePrix);
    }
    
    @Test
    public void remblettre1() {
    	assertEquals(lettre1.tarifRemboursement(),1.5f,tolerancePrix);
    }
    
    @Test
    public void remblettre2() {
    	assertEquals(lettre2.tarifRemboursement(),15.0f,tolerancePrix);
    }
    
    @Test
    public void urgencelettre1() {
    	assertEquals(lettre1.isUrgence(),false);
    }
    
    @Test
    public void urgencelettre2() {
    	assertEquals(lettre2.isUrgence(),true);
    }
    
    @Test
    public void tarif_base_lettre1() {
    	assertEquals(lettre1.getTarifBase(),0.5f);
    }
    
    @Test
    public void tarif_base_lettre2() {
    	assertEquals(lettre2.getTarifBase(),0.5f);
    }
    
    
}