package Examen.Examen;


import static org.junit.jupiter.api.Assertions.assertEquals;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import Examen.Examen.*;

class AppTestColis{
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	
	Colis colis1 = new Colis("Le pere Noel", 
			"famille Kaya, igloo 10, terres ouest",
			"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
	
	@Test 
    public void testcolis1()
    {
    	assertEquals("Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0",colis1.toString());
    }
	
	@Test
    public void affrancolis1() {
    	assertEquals(3.5f,colis1.tarifAffranchissement(),tolerancePrix);
    }
	
	@Test
    public void rembcolis1() {
    	assertEquals(colis1.tarifRemboursement(),100.0f,tolerancePrix);
    }
}